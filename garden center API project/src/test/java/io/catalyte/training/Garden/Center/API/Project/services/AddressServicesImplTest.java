package io.catalyte.training.Garden.Center.API.Project.services;

import io.catalyte.training.Garden.Center.API.Project.entities.Address;
import io.catalyte.training.Garden.Center.API.Project.repository.AddressRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class AddressServicesImplTest {

    @Mock
    AddressRepository addressRepository;

    @InjectMocks
    AddressServicesImpl addressServicesImpl;

    Address testAddress;
    List<Address> testList = new ArrayList<>();


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        testAddress = new Address(1L, "street", "city", "state", 14789L);
        testAddress.setId(1L);
        testList.add(testAddress);
    }

    @Test
    void getAllAddresses() {
        when(addressRepository.findAll()).thenReturn(testList);
        List<Address> result = addressServicesImpl.getAllAdresses();
        assertEquals(testList, result);
    }
}