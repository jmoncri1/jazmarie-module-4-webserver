package io.catalyte.training.Garden.Center.API.Project.services;

import io.catalyte.training.Garden.Center.API.Project.entities.Address;
import io.catalyte.training.Garden.Center.API.Project.entities.Customers;
import io.catalyte.training.Garden.Center.API.Project.entities.Products;
import io.catalyte.training.Garden.Center.API.Project.repository.ProductsRepository;
import io.catalyte.training.Garden.Center.API.Project.services.CustomerService;
import io.catalyte.training.Garden.Center.API.Project.services.OrderService;
import io.catalyte.training.Garden.Center.API.Project.services.ProductServiceImpl;
import io.catalyte.training.Garden.Center.API.Project.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ProductServiceImplTest {

    @Mock
    ProductsRepository productsRepository;

    @Mock
    CustomerService customerService;
    @Mock
    UserService userService;
    @Mock
    OrderService orderService;

    @InjectMocks
    ProductServiceImpl productServiceImpl;

    Products testProducts;
    List<Products> testList = new ArrayList<>();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        testProducts = new Products(1L, 1111, "immersion blender", "blender", "immersion", 99.99);
        testProducts.setId(1L);
        testList.add(testProducts);

        when(productsRepository.findById(1L)).thenReturn(Optional.ofNullable(testList.get(0)));
        //when(productsRepository.findByName(any(String.class))).thenReturn(Optional.ofNullable(testProducts));
        when(productsRepository.findBySku(any(Integer.class))).thenReturn(Optional.ofNullable(testProducts));
        when(productsRepository.saveAll(anyCollection())).thenReturn(testList);
        when(productsRepository.save(any(Products.class))).thenReturn(testList.get(0));
        when(productsRepository.findAll()).thenReturn(testList);
        when(productsRepository.findAll(any(Example.class))).thenReturn(testList);
        when(productsRepository.findByType(any(String.class))).thenReturn(Optional.ofNullable(testProducts));
        when(productsRepository.findByManufacturer(any(String.class))).thenReturn(testList);
    }


    @Test
    public void givenProductIdWhenFindByIdThenReturnProduct() {
        ResponseEntity<Products> products = productServiceImpl.getProductsById(1L);
        assertNotNull(products);

    }

    @Test
    void getAllProductsTest() {
        List<Products> result = productServiceImpl.getAllProducts();
        assertEquals(testList, result);
    }

    @Test
    void getProductsByIdTest() {
        ResponseEntity<Products> result = productServiceImpl.getProductsById(1L);
        Products retrievedProduct = result.getBody();
        assertEquals(testProducts, retrievedProduct);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }
    @Test
    void getProductsByIdNotFoundTest() {
        ResponseEntity<Products> result = productServiceImpl.getProductsById(2L);
        Products retrievedProduct = result.getBody();
        assertNull(retrievedProduct);
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

    @Test
    void getProductsBySku() {
        ResponseEntity<Products> result = productServiceImpl.getProductsBySku(1111);
        Products retrievedProduct = result.getBody();
        assertEquals(testProducts, retrievedProduct);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    void getProductsByType() {
        ResponseEntity<Products> result = productServiceImpl.getProductsByType("immersion blender");
        Products retrievedProduct = result.getBody();
        assertEquals(testProducts, retrievedProduct);
        assertEquals(HttpStatus.OK, result.getStatusCode());

    }



    @Test
    void getProductsByManufacturer() {
        List<Products> result = productServiceImpl.getProductsByManufacturer("immersion");
        Products retrievedProduct = result.get(0);
        assertEquals(testProducts, retrievedProduct);

    }

    @Test
    void getByPriceTest() {
        when(productsRepository.findByPrice(testProducts.getPrice())).thenReturn(testList);
        List<Products> result = productServiceImpl.getProductsByPrice(99.99);
        assertEquals(testList, result);
    }

    @Test
    void addProductTest(){
        Products productToAdd = new Products(2L, 2222, "tabletop", "grill", "tabletop grill", 85.99);
        when(productsRepository.save(productToAdd)).thenReturn(productToAdd);
        ResponseEntity<Products> result = productServiceImpl.addProducts(productToAdd);
        assertEquals(productToAdd, result.getBody());

    }
    @Test
    void addProductWithExistingSkuTest(){
        Products productToAdd = new Products(2L, 1111, "tabletop", "grill", "tabletop grill", 85.99);
        when(productsRepository.save(productToAdd)).thenReturn(productToAdd);
        ResponseEntity<Products> result = productServiceImpl.addProducts(productToAdd);
        assertEquals(productToAdd, productToAdd);

    }


    @Test
    void deleteProductById() {
        when(productsRepository.existsById(anyLong())).thenReturn(true);
        productServiceImpl.deleteProductById(1L);
        verify(productsRepository).deleteById(any());
    }
}
