package io.catalyte.training.Garden.Center.API.Project;



import static io.catalyte.training.Garden.Center.API.Project.constants.StringConstants.CONTEXT_CUSTOMERS;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class CustomersControllerTest {

}

//
//
//
//    ResultMatcher okStatus = MockMvcResultMatchers.status().isOk();
//    ResultMatcher createdStatus = MockMvcResultMatchers.status().isCreated();
//    ResultMatcher deletedStatus = MockMvcResultMatchers.status().isNotFound();
//    ResultMatcher expectedType = MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON);
//
//
//    @Autowired
//    private WebApplicationContext wac;
//    private MockMvc mockMvc;
//
//
//    @BeforeEach
//    public void setUp() {
//        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
//        this.mockMvc = builder.build();
//    }
//
//    @Test
//    public void getCustomersReturnAtLeastThree() throws Exception {
//        mockMvc
//                .perform(get(CONTEXT_CUSTOMERS))
//                .andExpect(okStatus)
//                .andExpect(expectedType)
//                .andExpectAll(jsonPath("$", hasSize(greaterThan(1))));
//    }
//
//    @Test
//    public void getCustomerThatExistById() throws Exception {
//        mockMvc
//                .perform(get(CONTEXT_CUSTOMERS + "/1"))
//                .andExpect(okStatus)
//                .andExpect(expectedType)
//                .andExpect(jsonPath("$.name", is("Joe Blow")));
//    }
//
//    @Test
//    public void postNewCustomer() throws Exception {
//        String json = "{\n" +
//                "        \"id\": 1,\n" +
//                "        \"name\": \"Joe Blow\",\n" +
//                "        \"email\": \"jblow@email.com\",\n" +
//                "        \"address\": {\n" +
//                "            \"id\": 1,\n" +
//                "            \"street\": \"235 Buyers Circle\",\n" +
//                "            \"city\": \"Baltimore\",\n" +
//                "            \"state\": \"Maryland\",\n" +
//                "            \"zipCode\": 12546\n" +
//                "        }";
//        this.mockMvc
//                .perform(post(CONTEXT_CUSTOMERS)
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(json))
//                .andExpect(createdStatus)
//                .andExpect(expectedType)
//                .andExpect(jsonPath("$.name", is("Joe Blow")));
//
//    }


