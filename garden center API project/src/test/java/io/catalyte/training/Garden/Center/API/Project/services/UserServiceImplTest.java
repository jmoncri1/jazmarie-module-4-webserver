package io.catalyte.training.Garden.Center.API.Project.services;

import io.catalyte.training.Garden.Center.API.Project.entities.Address;
import io.catalyte.training.Garden.Center.API.Project.entities.Customers;
import io.catalyte.training.Garden.Center.API.Project.entities.Users;
import io.catalyte.training.Garden.Center.API.Project.repository.UserRepository;
import io.catalyte.training.Garden.Center.API.Project.services.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserServiceImplTest {

    @Mock
    UserRepository userRepository;

    @Mock
    AddressService addressService;
    @Mock
    OrderService orderService;
    @Mock
    ProductService productService;


    @InjectMocks
    UserServiceImpl userServiceImpl;

    Users testUsers;
    List<Users> testList = new ArrayList<>();




    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        testUsers = new Users(1L, "UserOne", "Staff", new String[]{"Admin", "Secretary"}, "firstUser@email.com", "firstUser1");
        testUsers.setId(1L);
        testList.add(testUsers);

        when(userRepository.findById(any(Long.class))).thenReturn(Optional.of(testList.get(0)));
        when(userRepository.findByName(any(String.class))).thenReturn(Optional.ofNullable(testUsers));
        when(userRepository.getUsersByEmail(any(String.class))).thenReturn(Optional.ofNullable(testUsers));
        when(userRepository.saveAll(anyCollection())).thenReturn(testList);
        when(userRepository.save(any(Users.class))).thenReturn(testList.get(0));
        when(userRepository.findAll()).thenReturn(testList);
        when(userRepository.findAll(any(Example.class))).thenReturn(testList);
        when(userRepository.findByTitle(any(String.class))).thenReturn(testList);
        //when(userRepository.findUsersByRoles(any(Users.class).getRoles())).thenReturn(testList);
        when(userRepository.getUsersByPassword(any(String.class))).thenReturn(testList);
    }


    @Test
    void getAllUsers() {
        List<Users> result = userServiceImpl.getAllUsers();
        assertEquals(testList, result);
    }

    @Test
    void getUsersById() {
        ResponseEntity<Users> result = userServiceImpl.getUsers(1L);
        Users retrievedUser = result.getBody();
        assertEquals(testUsers, retrievedUser);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    void getUsersByName() {
        ResponseEntity<Users> result = userServiceImpl.getUsersByName("UserOne");
        Users retrievedUser = result.getBody();
        assertEquals(testUsers, retrievedUser);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    void getUsersByTitle() {
        List<Users> result = userServiceImpl.getUsersByTitle("Staff");
        assertEquals(testList, result);
    }

    @Test
    void getUsersByRole() {

    }

    @Test
    void getUsersByEmail() {
        Optional<Users> result = userServiceImpl.getUsersByEmail("FirstUser@email.com");
        assertEquals(testUsers, result.get());
    }

    @Test
    void getUsersByPassword() {
        List<Users> result = userServiceImpl.getUsersByPassword("FirstUser");
        assertEquals(testList, result);
    }

    @Test
    void addUsers() {
    }

    @Test
    void updateUsersById() {
    }

    @Test
    void testUpdateUsersById() {
    }

    @Test
    void deleteUsersById() {
        when(userRepository.existsById(anyLong())).thenReturn(true);
        userServiceImpl.deleteUsersById(1L);
        verify(userRepository).deleteById(any());
    }
}