package io.catalyte.training.Garden.Center.API.Project.services;

import io.catalyte.training.Garden.Center.API.Project.entities.Address;
import io.catalyte.training.Garden.Center.API.Project.entities.Customers;
import io.catalyte.training.Garden.Center.API.Project.entities.Orders;
import io.catalyte.training.Garden.Center.API.Project.entities.Products;
import io.catalyte.training.Garden.Center.API.Project.repository.OrdersRepository;
import org.aspectj.weaver.ast.Or;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class OrderServiceImplTest {

    @Mock
    OrdersRepository ordersRepository;

    @Mock
    CustomerService customerService;
    @Mock
    ProductService productService;
    @Mock
    AddressService addressService;

    @InjectMocks
    OrderServiceImpl orderServiceImpl;

    Orders testOrders;
    Customers testCustomers;
    Address testAddress;
    Products testProduct;
    List<Orders> testList = new ArrayList<>();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        testProduct = new Products(1L, 9999, "test type", "test name", "test manufacterer", 22.99);
        testAddress = new Address(1L, "test street", "testcity", "test state", 14789L);
        testCustomers = new Customers(1L, "test customer", "testemail@email.com", testAddress);
        testOrders = new Orders(1L, testCustomers, LocalDateTime.of(2015, 04, 30, 10, 55, 25), 299.99, testProduct, 5);
        testOrders.setId(1L);
        testList.add(testOrders);

        when(ordersRepository.findById(1L)).thenReturn(Optional.ofNullable(testList.get(0)));
        //when(.findByName(any(String.class))).thenReturn(Optional.ofNullable(testProducts));
        when(ordersRepository.findByCustomersId(any(Long.class))).thenReturn(testList);
        when(ordersRepository.saveAll(anyCollection())).thenReturn(testList);
        when(ordersRepository.save(any(Orders.class))).thenReturn(testList.get(0));
        when(ordersRepository.findAll()).thenReturn(testList);
        when(ordersRepository.findAll(any(Example.class))).thenReturn(testList);
        when(ordersRepository.findByOrderTotal(any(Double.class))).thenReturn(testList);
        when(ordersRepository.findByDate(any(LocalDateTime.class))).thenReturn(testList);
    }

    @Test
    void getAllOrders() {
        List<Orders> result = ordersRepository.findAll();
        assertEquals(testList, result);
    }

    @Test
    void getOrdersById() {
        ResponseEntity<Orders> result = orderServiceImpl.getOrdersById(1L);
//        Orders retrievedOrder = result.getBody();
        assertEquals(testOrders, testOrders);

    }


    @Test
    void getOrdersByCustomersId() {
    List<Orders> result = ordersRepository.findByCustomersId(1L);
    Orders retrievedProduct = result.get(0);
    assertEquals(testOrders, retrievedProduct);
    }

    @Test
    void getOrdersByDate() {
    List<Orders> result = ordersRepository.findByDate(LocalDateTime.of(2015, 04, 20, 10, 55, 25));
    Orders retrievedProduct = result.get(0);
    assertEquals(testOrders, retrievedProduct);
    }

    @Test
    void getOrdersByOrderTotal() {
    List<Orders> result = ordersRepository.findByOrderTotal(299.99);
    Orders retrievedProduct = result.get(0);
    assertEquals(testOrders, retrievedProduct);
    }

    @Test
    void getOrdersByProductsId() {
        when(ordersRepository.findByProductsId(1L)).thenReturn(testList);
    List<Orders> result = ordersRepository.findByProductsId(1L);
    assertEquals(testList, result);
    }

    @Test
    void getOrdersByQuantity() {
        when(ordersRepository.findByQuantity(testOrders.getQuantity())).thenReturn(testList);
   List<Orders> result = ordersRepository.findByQuantity(5);
    assertEquals(testList, result);
    }

    @Test
    void addOrders() {
    Orders orderToAdd = new Orders(2L, testCustomers, LocalDateTime.of(2022, 11, 21, 11, 11), 200.12, testProduct, 2);
    when(ordersRepository.save(orderToAdd)).thenReturn(orderToAdd);
    Orders result = orderServiceImpl.addOrders(orderToAdd);
    assertEquals(orderToAdd, orderToAdd);
    }

    @Test
    void updateOrders() {

    }

    @Test
    void deleteOrdersById() {
    when(ordersRepository.existsById(anyLong())).thenReturn(true);
    orderServiceImpl.deleteOrdersById(1L);
    verify(ordersRepository).deleteById(any());
    }
}