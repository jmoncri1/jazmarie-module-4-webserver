package io.catalyte.training.Garden.Center.API.Project.services;

import io.catalyte.training.Garden.Center.API.Project.entities.Address;
import io.catalyte.training.Garden.Center.API.Project.entities.Customers;
import io.catalyte.training.Garden.Center.API.Project.repository.CustomersRepository;
import io.catalyte.training.Garden.Center.API.Project.services.*;
import org.checkerframework.checker.units.qual.C;
import org.springframework.data.domain.Example;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyCollection;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import static org.junit.jupiter.api.Assertions.assertEquals;


public class CustomerServiceImplTest {

    @Mock
    CustomersRepository customersRepository;


    @Mock
    AddressService addressService;
    @Mock
    OrderService orderService;
    @Mock
    ProductService productService;
    @Mock
    UserService userService;

    @InjectMocks
    CustomerServiceImpl customerServiceImpl;

    Address testAddress;
    Customers testCustomers;
    List<Customers> testList = new ArrayList<>();


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        testAddress = new Address( 1L,"123 Sesame Street", "Atlanta", "GA", 12345L);
        testAddress.setId(1L);

        testCustomers = new Customers(1L, "plain jane", "Jane@email.com", testAddress);
        testCustomers.setId(1L);
        testList.add(testCustomers);

        when(customersRepository.findById(any(Long.class))).thenReturn(Optional.of(testList.get(0)));
        when(customersRepository.findByName(any(String.class))).thenReturn(Optional.ofNullable(testCustomers));
        when(customersRepository.findCustomerByEmail(any(String.class))).thenReturn(Optional.ofNullable(testCustomers));
        when(customersRepository.saveAll(anyCollection())).thenReturn(testList);
       // when(customersRepository.save(any(Customers.class))).thenReturn(testList.get(0));
        when(customersRepository.findAll()).thenReturn(testList);
        when(customersRepository.findAll(any(Example.class))).thenReturn(testList);
        when(customersRepository.findCustomerByAddress(any(Address.class))).thenReturn(testList);
        when(customersRepository.findCustomersByAddressStreet(any(String.class))).thenReturn(testList);
        when(customersRepository.findCustomersByAddressCity(any(String.class))).thenReturn(testList);
        when(customersRepository.findCustomersByAddressState(any(String.class))).thenReturn(testList);
        when(customersRepository.findCustomersByAddressZipCode(any(Long.class))).thenReturn(testList);

    }




    @Test
    void getAllCustomers() {
        List<Customers> result = customerServiceImpl.getAllCustomers();
        assertEquals(testList, result);
    }

    @Test
    void getCustomersById() {
       ResponseEntity <Customers> result = customerServiceImpl.getCustomersById(1L);
       Customers retrievedCustomer = result.getBody();
        assertEquals(testCustomers, retrievedCustomer);
        assertEquals(HttpStatus.OK, result.getStatusCode());

    }

    @Test
    void getCustomersByName() {
        ResponseEntity<Customers> result = customerServiceImpl.getCustomersByName("plain jane");
        Customers retrievedCustomer = result.getBody();
        assertEquals(testCustomers, retrievedCustomer);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    void getCustomersByEmail() {
        ResponseEntity<Customers> result = customerServiceImpl.getCustomersByEmail("jane@email.com");
        assertEquals(testCustomers, result.getBody());
    }

    @Test
    void getCustomersByAddress() {
        List<Customers> result = customerServiceImpl.getCustomersByAddress(testAddress);
        assertEquals(testList, result);
    }

    @Test
    void getCustomersByAddressStreet() {
        List<Customers> result = customerServiceImpl.getCustomersByAddressStreet("123 Sesame Street");
        assertEquals(testList, result);

    }

    @Test
    void getCustomersByAddressCity() {
        List<Customers> result = customerServiceImpl.getCustomersByAddressCity("Atlanta");
        assertEquals(testList, result);
    }

    @Test
    void getCustomerByAddressState() {
        List<Customers> result = customerServiceImpl.getCustomerByAddressState("GA");
        assertEquals(testList, result);
    }

    @Test
    void getCustomersByAddressZipCode() {
        List<Customers> result = customerServiceImpl.getCustomersByAddressZipCode(12345L);
        assertEquals(testList, result);
    }

    @Test
    void addCustomers() {
        Address addressToAdd = new Address(1L, "test street", "test city", "test state", 10478L);
        Customers customerToAdd = new Customers(2L, "Test Customer", "test@email.com", addressToAdd);
        when(customersRepository.save(any(Customers.class))).thenReturn(customerToAdd);
        ResponseEntity<Customers> result = customerServiceImpl.addCustomers(testCustomers);
        assertEquals(customerToAdd, customerToAdd);

    }

    @Test
    void updateCustomersById() {
    }

    @Test
    void deleteCustomerById() {
        when(customersRepository.existsById(anyLong())).thenReturn(true);
        customerServiceImpl.deleteCustomerById(1L);
        verify(customersRepository).deleteById(any());
    }
}
