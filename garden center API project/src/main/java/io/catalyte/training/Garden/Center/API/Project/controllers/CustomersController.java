package io.catalyte.training.Garden.Center.API.Project.controllers;


import io.catalyte.training.Garden.Center.API.Project.entities.Address;
import io.catalyte.training.Garden.Center.API.Project.entities.Customers;
import io.catalyte.training.Garden.Center.API.Project.repository.CustomersRepository;
import io.catalyte.training.Garden.Center.API.Project.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/customers/")
public class CustomersController {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomersRepository customersRepository;

    @GetMapping
    public List<Customers>getAllCustomers() {
        List<Customers> customers = customerService.getAllCustomers();

        return customers;
     }

     @GetMapping(value = "id/{id}")
    public ResponseEntity<Customers> getCustomersById(@PathVariable Long id) {
        return customerService.getCustomersById(id);
     }

     @GetMapping(value = "name/{name}")
    public ResponseEntity<Customers> getCustomersByName(@PathVariable String name) {
        return customerService.getCustomersByName(name);
        //return new ResponseEntity<Customers>(customerService.getCustomersByName(name).getStatusCode());
     }

     @GetMapping(value = "email/{email}")
    public ResponseEntity<Customers> getCustomersByEmail(@PathVariable String email) {
        return customerService.getCustomersByEmail(email);
     }

     @GetMapping(value = "address")
    public ResponseEntity<List<Customers>> getCustomersByAddress(@PathVariable Address address) {
         return new ResponseEntity<List<Customers>>(customerService.getCustomersByAddress(address), HttpStatus.OK);

     }

     @GetMapping(value = "address/{street}")
     @ResponseBody
    public List<Customers>getCustomersByAddressStreet(@PathVariable String street){
       List<Customers> customers = customerService.getCustomersByAddressStreet(street);
        return customers;
     }

     @GetMapping(value = "address/{city}")
    @ResponseBody
    public List<Customers> getCustomersByAddressCity(@PathVariable String city) {
        List<Customers> customers = customerService.getCustomersByAddressCity(city);
        return customers;
     }

     @GetMapping(value = "address/{state}")
    @ResponseBody
    public List<Customers> getCustomersByAddressState(@PathVariable String state) {
        List<Customers> customers = customerService.getCustomerByAddressState(state);
        return customers;
     }

     @GetMapping (value = "address/{zipCode}")
    @ResponseBody
    public List<Customers> getCustomersByAddressZipCode(@PathVariable Long zipCode) {
        List<Customers> customers = customerService.getCustomersByAddressZipCode(zipCode);
        return customers;
     }
     @PostMapping
    public ResponseEntity<Customers> saveNewCustomers(@RequestBody Customers customers) {
        //String regex = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
        // Pattern pattern = Pattern.compile(regex);
        // boolean matcher = Pattern.matches(customers.getEmail(), regex);
        if(customers.getEmail() == null
                || customers.getEmail().isEmpty()
                || (!customers.getEmail().contains("@"))){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
         }

        //if ((!matcher)) {
           // return new ResponseEntity<>(HttpStatus.CONFLICT);
      //  }

        return customerService.addCustomers(customers);
     }

     @PutMapping (value = "/{id}")
     @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Customers> updateCustomersById(@RequestBody Customers customers, @PathVariable Long id) {
        if(customers.getId() == null || customers.getName() == null || customers.getName().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return customerService.updateCustomersById(customers);
     }
     @DeleteMapping (value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCustomersById(@PathVariable Long id) {
        customerService.deleteCustomerById(id);
     }


}








