package io.catalyte.training.Garden.Center.API.Project.repository;


import io.catalyte.training.Garden.Center.API.Project.entities.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

Optional<Address> findById(Long id);
List<Address> findByStreet(String street);
List<Address> findAddressByCity(String city);
List<Address> findAddressByState(String state);
List<Address> findAddressByZipCode(Long zipCode);

}
