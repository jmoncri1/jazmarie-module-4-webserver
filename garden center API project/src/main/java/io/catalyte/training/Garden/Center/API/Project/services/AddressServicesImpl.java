package io.catalyte.training.Garden.Center.API.Project.services;


import io.catalyte.training.Garden.Center.API.Project.entities.Address;
import io.catalyte.training.Garden.Center.API.Project.repository.AddressRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServicesImpl implements AddressService {
    private final AddressRepository addressRepository;

    public AddressServicesImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public List<Address> getAllAdresses() {
        List<Address> addresses = addressRepository.findAll();
        return addresses;
    }
}
