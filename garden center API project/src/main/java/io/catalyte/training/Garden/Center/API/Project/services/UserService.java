package io.catalyte.training.Garden.Center.API.Project.services;

import io.catalyte.training.Garden.Center.API.Project.entities.Users;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<Users> getAllUsers();


    ResponseEntity<Users> getUsers(Long id);

    ResponseEntity<Users> getUsersByName(String name);

    List<Users> getUsersByTitle(String title);

    List<Users> getUsersByRole(String[] roles);
    Optional<Users>getUsersByEmail(String email);

    List<Users>getUsersByPassword(String password);

    ResponseEntity<Users> addUsers(Users users);

    ResponseEntity<Users> updateUsersById(Users newUsers, Long id);


    ResponseEntity<String> deleteUsersById(Long id);


}
