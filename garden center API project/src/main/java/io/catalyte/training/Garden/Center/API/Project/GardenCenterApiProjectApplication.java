package io.catalyte.training.Garden.Center.API.Project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GardenCenterApiProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(GardenCenterApiProjectApplication.class, args);
	}

}
