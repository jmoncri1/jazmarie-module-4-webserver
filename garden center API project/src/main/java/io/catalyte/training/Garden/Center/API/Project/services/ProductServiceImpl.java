package io.catalyte.training.Garden.Center.API.Project.services;


import io.catalyte.training.Garden.Center.API.Project.entities.Products;
import io.catalyte.training.Garden.Center.API.Project.repository.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * The type Product service.
 */
@Service/**/
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductsRepository productsRepository;

    /**
     * @return
     */
    @Override
    public List<Products> getAllProducts() {
        List<Products> products = productsRepository.findAll();
        return products;
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<Products> getProductsById(Long id) {
        Optional<Products> products = productsRepository.findById(id);
        if (products.isPresent()) {
            ResponseEntity<Products> responseEntity = new ResponseEntity<>(products.get(), HttpStatus.OK);
            return responseEntity;
        } else {
            ResponseEntity<Products> responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            return responseEntity;
        }
    }

    /**
     * @param sku
     * @return
     */
    @Override
    public ResponseEntity<Products> getProductsBySku(Integer sku) {
        Optional<Products> products = productsRepository.findBySku(sku);
        ResponseEntity<Products> responseEntity = new ResponseEntity<>(products.get(), HttpStatus.OK);
        return responseEntity;
    }

    /**
     * @param type
     * @return
     */
    @Override
    public ResponseEntity<Products> getProductsByType(String type) {
        Optional<Products> products = productsRepository.findByType(type);
        ResponseEntity<Products> responseEntity = new ResponseEntity<>(products.get(), HttpStatus.OK);
        return responseEntity;
    }

    /**
     * @param name
     * @return
     */
    @Override
    public ResponseEntity<Products> getProductsByName(String name) {
        Optional<Products> products = productsRepository.findByName(name);
        return new ResponseEntity<>(products.get(), HttpStatus.OK);

    }

    /**
     * @param manufacturer
     * @return
     */
    @Override
    public List<Products> getProductsByManufacturer(String manufacturer) {
        List<Products> products = productsRepository.findByManufacturer(manufacturer);
        return products;
    }

    /**
     * @param price
     * @return
     */
    @Override
    public List<Products> getProductsByPrice(Double price) {
        List<Products> products = productsRepository.findByPrice(price);
        return products;
    }

    /**
     * @param products
     * @return
     */
    @Override
    public ResponseEntity<Products> addProducts(Products products) {
        Optional<Products> possibleProduct = (productsRepository.findBySku(products.getSku()));
        if (possibleProduct.isPresent()) {
            ResponseEntity<Products> responseEntity = new ResponseEntity<>(HttpStatus.CONFLICT);
            return responseEntity;
        } else if (products.getSku() == null
                || products.getType() == null
                || products.getName() == null
                || products.getManufacturer() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            Products savedProducts = productsRepository.save(products);
            ResponseEntity<Products> responseEntity = new ResponseEntity<>(savedProducts, HttpStatus.CREATED);
            return responseEntity;
        }
    }
//        Optional<Products> possibleProduct = productsRepository.findBySku(products.getSku());
//        if(possibleProduct.isPresent()) {
//            ResponseEntity<Products> responseEntity = new ResponseEntity<>(HttpStatus.CONFLICT);
//            return responseEnti

    /**
     * @param products
     * @return
     */
    @Override
    public ResponseEntity<Products> updateProducts(Products products) {
        Optional<Products> possibleProduct = productsRepository.findById(products.getId());

        Optional<Products> possibleProductForSkuCheck = productsRepository.findBySku(products.getSku());
        ResponseEntity<Products> responseEntity;
        if (!possibleProduct.isPresent()) {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            // } else if (possibleProductForSkuCheck.isPresent()) {
            // return new ResponseEntity<>(HttpStatus.CONFLICT);
        } else if (products.getSku() == null
                || products.getType() == null
                || products.getName() == null
                || products.getManufacturer() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//        } else if (possibleProduct.get().getSku() != ) {
//            return new ResponseEntity<>(HttpStatus.CONFLICT);
       }
            Products updatedProducts = productsRepository.save(products);
            return new ResponseEntity<>(updatedProducts, HttpStatus.OK);

        }


    /**
     * @param id
     */
    @Override
    public ResponseEntity<String> deleteProductById(Long id) {
        try {
            if (productsRepository.existsById(id)) {
                productsRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}