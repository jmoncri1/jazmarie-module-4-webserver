package io.catalyte.training.Garden.Center.API.Project.services;

import io.catalyte.training.Garden.Center.API.Project.entities.Address;
import io.catalyte.training.Garden.Center.API.Project.entities.Customers;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CustomerService {

    List<Customers> getAllCustomers();
    ResponseEntity<Customers> getCustomersById(Long id);
    ResponseEntity<Customers> getCustomersByName(String name);
    ResponseEntity<Customers> getCustomersByEmail(String email);
    List<Customers> getCustomersByAddress(Address address);
    List<Customers> getCustomersByAddressStreet(String street);
    List<Customers> getCustomersByAddressCity(String city);
    List<Customers> getCustomerByAddressState(String state);
    List<Customers> getCustomersByAddressZipCode(Long zipCode);

    ResponseEntity<Customers> addCustomers(Customers customers);

    ResponseEntity<Customers> updateCustomersById(Customers customers);


    ResponseEntity<String> deleteCustomerById(Long id);
}
