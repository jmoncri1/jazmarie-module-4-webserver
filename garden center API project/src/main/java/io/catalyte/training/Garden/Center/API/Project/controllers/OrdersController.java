package io.catalyte.training.Garden.Center.API.Project.controllers;


import io.catalyte.training.Garden.Center.API.Project.entities.Customers;
import io.catalyte.training.Garden.Center.API.Project.entities.Orders;
import io.catalyte.training.Garden.Center.API.Project.repository.OrdersRepository;
import io.catalyte.training.Garden.Center.API.Project.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/orders/")
public class OrdersController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrdersRepository ordersRepository;

    @GetMapping
    public List<Orders> getAllOrders() {
        List<Orders> orders = orderService.getAllOrders();
        return orders;
    }

    @GetMapping(value = ("id/{id}"))
    public ResponseEntity<Orders> getOrdersById(@PathVariable Long id) {
        return orderService.getOrdersById(id);
    }

    @GetMapping(value = ("customerId/{customerId}"))
    public List<Orders> getOrdersByCustomerId(@PathVariable Long id) {
        List<Orders> orders = orderService.getOrdersByCustomersId(id);
        return orders;
    }

    @GetMapping(value = ("date/{date}"))
    public List<Orders> getOrdersByDate(@PathVariable LocalDateTime date) {
        List<Orders> orders = orderService.getOrdersByDate(date);
        return orders;
    }

    @GetMapping(value = ("orderTotal/{orderTotal}"))
    public List<Orders> getOrdersByOrderTotal(@PathVariable Double orderTotal) {
        List<Orders> orders = orderService.getOrdersByOrderTotal(orderTotal);
        return orders;
    }

    @GetMapping(value = ("order/{productId}"))
    @ResponseBody
    public List<Orders> getOrdersByProductId(@PathVariable Long id) {
        List<Orders> orders = orderService.getOrdersByProductsId(id);
        return orders;
    }

    @GetMapping(value = ("quantity/{quantity}"))
    public List<Orders> getOrdersByQuantity(@PathVariable Integer quantity) {
        List<Orders> orders = orderService.getOrdersByQuantity(quantity);
        return orders;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Orders saveNewOrders(@RequestBody Orders orders) {
        return orderService.addOrders(orders);
    }

    @PutMapping
    public Orders updateOrdersById(@RequestBody Orders orders) {
        return orderService.updateOrders(orders);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<String> deleteOrdersById(@PathVariable Long id) {
        return orderService.deleteOrdersById(id);
    }
}







