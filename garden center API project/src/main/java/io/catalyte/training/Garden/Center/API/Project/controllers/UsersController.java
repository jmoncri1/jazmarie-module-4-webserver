package io.catalyte.training.Garden.Center.API.Project.controllers;


import io.catalyte.training.Garden.Center.API.Project.entities.Users;
import io.catalyte.training.Garden.Center.API.Project.repository.UserRepository;
import io.catalyte.training.Garden.Center.API.Project.services.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users/")
public class UsersController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<Users> getAllUsers() {
        List<Users> users = userService.getAllUsers();
        return users;
    }

    @GetMapping(value = "id/{id}")
    public ResponseEntity<Users> getUsers(@PathVariable Long id) {
        return userService.getUsers(id);
        //return new ResponseEntity<Users>((MultiValueMap<String, String>) userService.getUsers(id), HttpStatus.OK);

    }

    @GetMapping(value = "name/{name}")
    public ResponseEntity<Users> getUsersByName(@PathVariable String name) {
        return userService.getUsersByName(name);
        //return new ResponseEntity<Users>(userService.getUsersByName(name).getStatusCode());
    }

    @GetMapping(value = "title/{title}")
    public ResponseEntity<List<Users>> getUsersByTitle(@PathVariable String title) {
        return new ResponseEntity<List<Users>>(userService.getUsersByTitle(title), HttpStatus.OK);
    }

    @GetMapping(value = "role/{roles}")
    public ResponseEntity<List<Users>> getUsersByRole(@PathVariable String[] roles) {
        return new ResponseEntity<List<Users>>(userService.getUsersByRole(roles), HttpStatus.OK);
    }

    @GetMapping(value = "email/{email}")
    public Optional<Users> getUsersByEmail(@PathVariable String email) {
        return userService.getUsersByEmail(email);
    }

    @GetMapping(value = "password/{password}")
    public ResponseEntity<List<Users>> getUserByPassword(@PathVariable String password) {
        return new ResponseEntity<List<Users>>(userService.getUsersByPassword(password), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Users> saveNewUsers(@RequestBody Users users) {
            return userService.addUsers(users);
        }



    @PutMapping(value = "/{id}")
    public ResponseEntity<Users> updateUsersById(@RequestBody Users newUsers, @PathVariable Long id) {
        if(newUsers.getId() == id) {
            return userService.updateUsersById(newUsers, id);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<String> deleteUserById(@PathVariable Long id) {
       return userService.deleteUsersById(id);
    }
}

//    @DeleteMapping(value = "/{id}")
//    //@ResponseStatus(HttpStatus.NO_CONTENT)
//    public ResponseEntity deleteUsersById(@PathVariable Long id) {
//
//            UserService.deleteUserById(id);
//
//        return new ResponseEntity(HttpStatus.NOT_FOUND);
//
//      // return userService.deleteUsersById(id);


//    public void deleteUsersById(@PathVariable Long id) {
//        userService.deleteUsersById(id);
//    }
//}

//    @DeleteMapping(value = "/{id}")
//    public ResponseEntity deleteVaccinationById(@PathVariable Long id) {
//        logger.info(new Date() + " Delete request received for id: " + id);
//        VaccinationService.deleteVaccination(id);
//        return new ResponseEntity(HttpStatus.NO_CONTENT);
//    }

