package io.catalyte.training.Garden.Center.API.Project.services;

import io.catalyte.training.Garden.Center.API.Project.entities.Customers;
import io.catalyte.training.Garden.Center.API.Project.entities.Users;
import io.catalyte.training.Garden.Center.API.Project.repository.OrdersRepository;
import io.catalyte.training.Garden.Center.API.Project.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The type User service.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private OrdersRepository ordersRepository;


    @Override
    public List<Users> getAllUsers() {
        List<Users> users = userRepository.findAll();
        return users;
    }

    @Override
    public ResponseEntity<Users> getUsers(Long id) {
        Optional<Users> users = userRepository.findById(id);
        if (users.isPresent()) {
            ResponseEntity<Users> responseEntity = new ResponseEntity<>(users.get(), HttpStatus.OK);
            return responseEntity;
        } else {
            ResponseEntity<Users> responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            return responseEntity;
        }

    }

    @Override
    public ResponseEntity<Users> getUsersByName(String name) {
        Optional<Users> users = userRepository.findByName(name);
        if (users.isPresent()) {
            ResponseEntity<Users> responseEntity = new ResponseEntity<>(users.get(), HttpStatus.OK);
            return responseEntity;
        } else {
            ResponseEntity<Users> responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            return responseEntity;
        }
    }

    @Override
    public List<Users> getUsersByTitle(String title) {
        List<Users> users = userRepository.findByTitle(title);
        return users;
    }

    @Override
    public List<Users> getUsersByRole(String[] roles) {
        List<Users> users = userRepository.findUsersByRoles(roles);
        return users;
    }

    @Override
    public Optional<Users> getUsersByEmail(String email) {
        Optional<Users> users = null;
        return users = userRepository.getUsersByEmail(email);
    }

    @Override
    public List<Users> getUsersByPassword(String password) {
        List<Users> users = userRepository.getUsersByPassword(password);
        return users;
    }

    @Override
    public ResponseEntity<Users> addUsers(Users users) {
        Optional<Users> oldUser = (userRepository.getUsersByEmail(users.getEmail()));
        if (!users.getEmail().contains("@")
                || users.getName().isEmpty()
                || users.getTitle().isEmpty()
                || users.getPassword().length() < 8) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (oldUser.isPresent()) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } else {//
//        } else if (oldUser.isEmpty()) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        } else if (oldUser.get().getEmail() != newUsers.getEmail()) {
//            return new ResponseEntity<>(HttpStatus.CONFLICT);
//        }
            Users updatedUser = userRepository.save(users);
            return new ResponseEntity<>(users, HttpStatus.CREATED);
        }
    }

    /**
     * @param
     * @return
     */
    @Override
    public ResponseEntity<Users> updateUsersById(Users newUsers, Long id) {
        Optional<Users> oldUser = (userRepository.getUsersByEmail(newUsers.getEmail()));
        if (!newUsers.getEmail().contains("@")
                || newUsers.getName().isEmpty()
                || newUsers.getTitle().isEmpty()
                || newUsers.getPassword().length() < 8) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (oldUser.isPresent()) {
            if (oldUser.get().getId() != newUsers.getId()) {
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
        }

        return new ResponseEntity<>(newUsers, HttpStatus.OK);
    }



//    @Override
//    public ResponseEntity<Users> updateUsersById(Long id) {
//      Optional<Users> possibleUser = userRepository.findById(id);
//        if(possibleUser.isPresent()) {
//            Users savedUser = userRepository.save(users);
//            ResponseEntity<Users> responseEntity = new ResponseEntity<>(savedUser, HttpStatus.OK);
//            return responseEntity;
//        }else {
//            ResponseEntity<Users> responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
//            return responseEntity;
//        }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<String> deleteUsersById(Long id) {
        try {
            if (userRepository.existsById(id)) {
                userRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>("User with that ID does not exist", HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

//try {
//        if (vaccinationRepository.existsById(id)) {
//        vaccinationRepository.deleteById(id);
//        return;
//        }
//        } catch (Exception e) {
//        throw new ServiceUnavailable(e);
//        }