package io.catalyte.training.Garden.Center.API.Project.controllers;


import io.catalyte.training.Garden.Center.API.Project.entities.Products;
import io.catalyte.training.Garden.Center.API.Project.repository.ProductsRepository;
import io.catalyte.training.Garden.Center.API.Project.services.ProductService;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/products/")
public class ProductsController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductsRepository productsRepository;



    @GetMapping
    public List<Products> getAllProducts() {
        List<Products> products = productService.getAllProducts();
        return products;
    }

    @GetMapping(value = "id/{id}")
    public ResponseEntity<Products> getProductsById(@PathVariable Long id) {
        return productService.getProductsById(id);
    }

    @GetMapping(value = "sku/{sku}")
    public ResponseEntity<Products> getProductsBySku(@PathVariable Integer sku) {
        return productService.getProductsBySku(sku);
    }

    @GetMapping(value = "type/{type}")
    public ResponseEntity<Products> getProductsByType(@PathVariable String type) {
        return new ResponseEntity<Products>((MultiValueMap<String, String>) productService.getProductsByType(type), HttpStatus.OK);
    }

    @GetMapping(value = "name/{name}")
    public ResponseEntity<Products> getProductsByName(@PathVariable String name) {
        return new ResponseEntity<Products>(productService.getProductsByName(name).getStatusCode());
    }

    @GetMapping("manufacturer/{manufacturer}")
    public ResponseEntity<List<Products>> getProductsByManufacturer(@PathVariable String manufacturer) {
        return new ResponseEntity<List<Products>>(productService.getProductsByManufacturer(manufacturer), HttpStatus.OK);
    }

    @GetMapping(value = "price/{price}")
    public ResponseEntity<List<Products>> getProductsByPrice(@PathVariable Double price) {
        return new ResponseEntity<List<Products>>(productService.getProductsByPrice(price), HttpStatus.OK);
    }

    @PostMapping
    @ResponseStatus
    public ResponseEntity<Products> saveNewProducts(@RequestBody Products products) {
        return productService.addProducts(products);
    }

    @PutMapping
    public ResponseEntity<Products> updateProductsById(@RequestBody Products products) {
        return productService.updateProducts(products);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @OnDelete(action = OnDeleteAction.CASCADE)
    public void deleteProductsById(@PathVariable Long id) {
        productService.deleteProductById(id);
    }










}
