package io.catalyte.training.Garden.Center.API.Project.services;

import io.catalyte.training.Garden.Center.API.Project.entities.Orders;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderService {
    List<Orders> getAllOrders();
    ResponseEntity<Orders> getOrdersById(Long id);
    List<Orders>getOrdersByCustomersId(Long id);
    List<Orders>getOrdersByDate(LocalDateTime date);
    List<Orders>getOrdersByOrderTotal(Double orderTotal);
    List<Orders>getOrdersByProductsId(Long id);
    List<Orders>getOrdersByQuantity(Integer quantity);
    Orders addOrders(Orders orders);
    Orders updateOrders(Orders orders);
    ResponseEntity<String> deleteOrdersById(Long id);
}
