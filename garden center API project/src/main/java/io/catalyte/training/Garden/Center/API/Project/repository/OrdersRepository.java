package io.catalyte.training.Garden.Center.API.Project.repository;

import io.catalyte.training.Garden.Center.API.Project.entities.Customers;
import io.catalyte.training.Garden.Center.API.Project.entities.Orders;
import io.catalyte.training.Garden.Center.API.Project.entities.Products;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface OrdersRepository extends JpaRepository<Orders, Long> {

    Optional<Orders>findById(Long id);
    List<Orders> findByCustomersId(Long id);
    List<Orders>findByDate(LocalDateTime date);
    List<Orders>findByOrderTotal(Double orderTotal);
    List<Orders>findByProductsId(Long id);
    List<Orders>findByQuantity(Integer quantity);
}
