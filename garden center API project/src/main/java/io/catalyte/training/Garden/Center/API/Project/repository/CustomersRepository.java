package io.catalyte.training.Garden.Center.API.Project.repository;


import io.catalyte.training.Garden.Center.API.Project.entities.Address;
import io.catalyte.training.Garden.Center.API.Project.entities.Customers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CustomersRepository extends JpaRepository<Customers, Long> {

    Optional<Customers> findByName(String name);

    Optional<Customers> findCustomerByEmail(String email);

   List<Customers>findCustomerByAddress(Address address);
   List<Customers>findCustomersByAddressStreet(String street);
   List<Customers>findCustomersByAddressCity(String city);
   List<Customers>findCustomersByAddressState(String state);
   List<Customers>findCustomersByAddressZipCode(Long zipCode);






}
