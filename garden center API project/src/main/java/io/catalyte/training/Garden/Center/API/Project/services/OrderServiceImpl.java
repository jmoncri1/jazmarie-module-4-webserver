package io.catalyte.training.Garden.Center.API.Project.services;

import io.catalyte.training.Garden.Center.API.Project.entities.Orders;
import io.catalyte.training.Garden.Center.API.Project.repository.OrdersRepository;
import io.catalyte.training.Garden.Center.API.Project.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {
    private final OrdersRepository ordersRepository;

    public OrderServiceImpl(OrdersRepository ordersRepository) {
        this.ordersRepository = ordersRepository;
    }

    /**
     * @return
     */
    @Override
    public List<Orders> getAllOrders() {
        List<Orders> orders = ordersRepository.findAll();
        return orders;
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<Orders> getOrdersById(Long id) {
        Optional<Orders> orders = ordersRepository.findById(id);
        if (orders.isPresent()) {
            ResponseEntity<Orders> responseEntity = new ResponseEntity<>(HttpStatus.OK);
            return responseEntity;
        } else {
            ResponseEntity<Orders> responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            return responseEntity;
        }
    }

    /**
     * @param id
     * @return
     */
    @Override
    public List<Orders> getOrdersByCustomersId(Long id) {
        List<Orders> orders = ordersRepository.findByCustomersId(id);
        return orders;
    }

    /**
     * @param date
     * @return
     */
    @Override
    public List<Orders> getOrdersByDate(LocalDateTime date) {
        List<Orders> orders = null;
        return orders = ordersRepository.findByDate(date);
    }

    @Override
    public List<Orders> getOrdersByOrderTotal(Double orderTotal) {
        List<Orders> orders = null;
        return orders = ordersRepository.findByOrderTotal(orderTotal);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public List<Orders> getOrdersByProductsId(Long id) {
        List<Orders> orders = null;
        return orders = ordersRepository.findByProductsId(id);
    }

    /**
     * @param quantity
     * @return
     */
    @Override
    public List<Orders> getOrdersByQuantity(Integer quantity) {
        List<Orders> orders = null;
        return orders = ordersRepository.findByQuantity(quantity);
    }

    /**
     * @param orders
     * @return
     */
    @Override
    public Orders addOrders(Orders orders) {
        return ordersRepository.save(orders);
    }

    /**
     * @param orders
     * @return
     */
    @Override
    public Orders updateOrders(Orders orders) {
        return ordersRepository.save(orders);
    }

    /**
     * @param id
     */
    @Override
    public ResponseEntity<String> deleteOrdersById(Long id) {
        try {
            if (ordersRepository.existsById(id)) {
                ordersRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);

            } else {
                return new ResponseEntity<>("Order with that ID does no exist", HttpStatus.NOT_FOUND);

            }
        } catch (Exception e) {
            throw new RuntimeException();
        }

        }
    }