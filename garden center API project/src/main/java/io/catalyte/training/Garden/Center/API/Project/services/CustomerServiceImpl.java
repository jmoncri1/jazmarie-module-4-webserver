package io.catalyte.training.Garden.Center.API.Project.services;

import io.catalyte.training.Garden.Center.API.Project.entities.Address;
import io.catalyte.training.Garden.Center.API.Project.entities.Customers;
import io.catalyte.training.Garden.Center.API.Project.repository.CustomersRepository;
import io.catalyte.training.Garden.Center.API.Project.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomersRepository customersRepository;
    @Autowired
    private UserRepository userRepository;

    /**
     * @return
     */
    @Override
    public List<Customers> getAllCustomers() {
        List<Customers> customers = customersRepository.findAll();
        return customers;
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<Customers> getCustomersById(Long id) {
        Optional<Customers> customers = customersRepository.findById(id);
        if (customers.isPresent()) {
            ResponseEntity<Customers> responseEntity = new ResponseEntity<>(customers.get(), HttpStatus.OK);
            return responseEntity;
        } else {
            ResponseEntity<Customers> responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            return responseEntity;
        }

    }

    /**
     * @param name
     * @return
     */
    @Override
    public ResponseEntity<Customers> getCustomersByName(String name) {
       Optional<Customers> customers = customersRepository.findByName(name);
        if (customers.isPresent()) {
           ResponseEntity<Customers> responseEntity = new ResponseEntity<>(customers.get(), HttpStatus.OK);
            return responseEntity;
        } else {
            ResponseEntity<Customers> responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            return responseEntity;
        }
    }

    /**
     * @param email
     * @return
     */
    @Override
    public ResponseEntity<Customers> getCustomersByEmail(String email) {
        Optional<Customers> customers = customersRepository.findCustomerByEmail(email);
        ResponseEntity<Customers> responseEntity = new ResponseEntity<>(customers.get(), HttpStatus.OK);
        return responseEntity;
    }

    /**
     * @param address
     * @return
     */
    @Override
    public List<Customers> getCustomersByAddress(Address address) {
        List<Customers> customers = customersRepository.findCustomerByAddress(address);
        return customers;
    }

    /**
     * @param street
     * @return
     */
    @Override
    @Transactional
    public List<Customers> getCustomersByAddressStreet(String street) {
        List<Customers> customers = customersRepository.findCustomersByAddressStreet(street);
        return customers;
    }

    /**
     * @param city
     * @return
     */
    @Override
    public List<Customers> getCustomersByAddressCity(String city) {
        List<Customers> customers = customersRepository.findCustomersByAddressCity(city);
        return customers;
    }

    /**
     * @param state
     * @return
     */
    @Override
    public List<Customers> getCustomerByAddressState(String state) {
        List<Customers> customers = customersRepository.findCustomersByAddressState(state);
        return customers;
    }

    /**
     * @param zipCode
     * @return
     */
    @Override
    public List<Customers> getCustomersByAddressZipCode(Long zipCode) {
        List<Customers> customers = customersRepository.findCustomersByAddressZipCode(zipCode);
        return customers;
    }

    /**
     * @param customers
     * @return
     */
    @Override
    public ResponseEntity<Customers> addCustomers(Customers customers) {
        Optional<Customers> possibleCustomer = customersRepository.findCustomerByEmail(customers.getEmail());
        if (possibleCustomer.isPresent()) {
            ResponseEntity<Customers> responseEntity = new ResponseEntity<>(HttpStatus.CONFLICT);
            return responseEntity;
        } else {
            Customers savedCustomer = customersRepository.save(customers);
            ResponseEntity<Customers> responseEntity = new ResponseEntity<>(savedCustomer, HttpStatus.CREATED);
            return responseEntity;
        }

    }

    /**
     * @param customers
     * @return
     */
    @Override
    public ResponseEntity<Customers> updateCustomersById(Customers customers) {
        Optional<Customers> possibleCustomer = customersRepository.findById(customers.getId());

        ResponseEntity<Customers> responseEntity;
        if (!possibleCustomer.isPresent()) {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Optional<Customers> nextCustomerCheck = customersRepository.findCustomerByEmail(customers.getEmail());
        if (nextCustomerCheck.isPresent()) {
            responseEntity = new ResponseEntity<>(HttpStatus.CONFLICT);
        } else {
            Customers savedCustomer = customersRepository.save(customers);
            responseEntity = new ResponseEntity<>(savedCustomer, HttpStatus.OK);
        }
        return responseEntity;
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<String> deleteCustomerById(Long id) {
        try {
            if (customersRepository.existsById(id)) {
                customersRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>("Customer with ID does not exist", HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
