package io.catalyte.training.Garden.Center.API.Project.services;

import io.catalyte.training.Garden.Center.API.Project.entities.Address;

import java.util.List;

public interface AddressService {

    List<Address> getAllAdresses();
}
