package io.catalyte.training.Garden.Center.API.Project.entities;


import javax.persistence.*;

@Entity
public class Products {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private Integer sku;
    private String type;
    private String name;
    private String manufacturer;
    private Double price;

    public Products(){}

    public Products(Long id, Integer sku, String type, String name, String manufacturer, Double price) {
        this.id = id;
        this.sku = sku;
        this.type = type;
        this.name = name;
        this.manufacturer = manufacturer;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSku() {
        return sku;
    }

    public void setSku(Integer sku) {
        this.sku = sku;
    }
    public String getType() {return type;}
    public void setType(String type) {this.type = type;}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
