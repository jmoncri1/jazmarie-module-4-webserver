package io.catalyte.training.Garden.Center.API.Project.services;

import io.catalyte.training.Garden.Center.API.Project.entities.Products;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProductService {

    List<Products>getAllProducts();
    ResponseEntity<Products> getProductsById(Long id);
    ResponseEntity<Products>getProductsBySku(Integer sku);
    ResponseEntity<Products>getProductsByType(String type);
    ResponseEntity<Products>getProductsByName(String name);
    List<Products>getProductsByManufacturer(String manufacturer);
    List<Products>getProductsByPrice(Double price);

    ResponseEntity<Products> addProducts(Products products);

    ResponseEntity<Products> updateProducts(Products products);

    ResponseEntity<String> deleteProductById(Long id);
}
