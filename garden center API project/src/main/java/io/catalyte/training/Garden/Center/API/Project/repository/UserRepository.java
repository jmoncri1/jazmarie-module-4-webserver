package io.catalyte.training.Garden.Center.API.Project.repository;


import io.catalyte.training.Garden.Center.API.Project.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {
//    @Transactional
//    @Modifying
//    @Query("update Users u set u.id = ?1 where u.id = ?2")
//    //int updateById(Long id);

   Optional<Users> findByName(String name);

    Optional<Users> findById(Long id);

    List<Users> findByTitle(String title);

    List<Users> findUsersByRoles(String[] roles);

    Optional<Users> getUsersByEmail(String email);

    List<Users> getUsersByPassword(String password);


}
