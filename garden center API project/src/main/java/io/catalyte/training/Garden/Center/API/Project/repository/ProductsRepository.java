package io.catalyte.training.Garden.Center.API.Project.repository;

import io.catalyte.training.Garden.Center.API.Project.entities.Products;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductsRepository extends JpaRepository<Products, Long> {

    Optional<Products> findById(Long id);
    Optional<Products>findBySku(Integer sku);
    Optional<Products>findByType(String type);
    Optional<Products>findByName(String name);
    List<Products>findByManufacturer(String manufacturer);
    List<Products>findByPrice(Double price);


}
