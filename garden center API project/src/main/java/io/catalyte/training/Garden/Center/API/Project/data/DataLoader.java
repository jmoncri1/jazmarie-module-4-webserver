package io.catalyte.training.Garden.Center.API.Project.data;

import io.catalyte.training.Garden.Center.API.Project.entities.*;
import io.catalyte.training.Garden.Center.API.Project.repository.*;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

@Component
public class DataLoader implements CommandLineRunner {

   // private final Logger logger = (Logger) LoggerFactory.getLogger(DataLoader.class);
    @Autowired
    private UserRepository userRepository;

    private Users users1;
    private Users users2;
    private Users users3;

    @Autowired
    private CustomersRepository customersRepository;

    private Customers customers1;
    private Customers customers2;
    private Customers customers3;

    @Autowired
    private OrdersRepository ordersRepository;

    private Orders orders1;
    private Orders orders2;
    private Orders orders3;
    private Orders orders4;
    private Orders orders5;

    @Autowired
    private AddressRepository addressRepository;

    private Address address1;
    private Address address2;
    private Address address3;

    @Autowired
    private ProductsRepository productsRepository;

    private Products products1;
    private Products products2;
    private Products products3;

    @Override
    public void run(String... args) throws Exception {
        //logger.info("Loading data...");

        loadUsers();
        loadAddress();
        loadCustomers();
        loadProducts();
        loadOrders();



    }


    private void loadUsers() {
        users1 = userRepository.save(new Users(1L, "FirstUser", "Secretary", new String[]{"Secretary", "Employee"}, "FirstUser@email.com", "UserOne" ));
        users2 = userRepository.save(new Users(2L, "SecondUser", "Administrator", new String[] {"Administrator", "Faculty"}, "SecondUser@email.com", "UserTwo"));
        users3 = userRepository.save(new Users(3L, "ThirdUser", "Manager", new String[]{"Manager", "Staff"}, "ThirdUser@email.com", "UserThree"));

    }

    private void loadAddress() {
        address1 = addressRepository.save(new Address(1L, "235 Buyers Circle", "Baltimore", "Maryland", 12546L));
        address2 = addressRepository.save(new Address(2L, "485 Kings Street", "Philadelphia", "Pennsylvania", 23456L));
        address3 = addressRepository.save(new Address(3L, "859 Engine blvd", "Bronx", "New York", 10063L));
    }
    private void loadCustomers() {
        customers1 = customersRepository.save(new Customers(1L, "Joe Blow", "jblow@email.com", address1 ));
        customers2 = customersRepository.save(new Customers(2L, "Jane Payne", "jpayne@email.com", address2));
        customers3 = customersRepository.save(new Customers(3L, "Mike Hike", "mhike@email.com", address3));
    }

    private void loadOrders() {
        orders1 = ordersRepository.save(new Orders(1L, customers1, LocalDateTime.of(2020, 12, 22, 11, 55), 49.99, products1, 1));
        orders2 = ordersRepository.save(new Orders(2L, customers2, LocalDateTime.of(2020, 4, 15, 10, 15), 199.99, products2, 1));
        orders3 = ordersRepository.save(new Orders(3L, customers3, LocalDateTime.of(2019, 11, 03, 04, 22), 99.99, products3, 1));
        orders4 = ordersRepository.save(new Orders(4L, customers3, LocalDateTime.of(2017, 07, 02, 06, 44), 49.99, products1, 1));
        orders5 = ordersRepository.save(new Orders(5L, customers1, LocalDateTime.of(2022, 05, 22, 11, 26), 199.99, products2, 1));

    }

    private void loadProducts() {
        products1 = productsRepository.save(new Products(1L, 1111, "immersion", "immersionBlender", "ninja", 49.99));
        products2 = productsRepository.save(new Products(2L, 2222, "counter", "counterBlender", "Oyster", 199.99));
        products3 = productsRepository.save(new Products(3L, 3333, "portable", "portableBlender", "kitchenAid", 99.99));
    }


}
