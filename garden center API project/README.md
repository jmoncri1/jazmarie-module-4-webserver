# Garden Center API Project

## By Jazmarie Moncrieft
### Garden Center Description

#### Garden Center LLC is a regional home and garden supply chain. The majority of their
revenue come from small business owners purchasing supplies at the local store.
Based on feedback, Garden Center LLC is looking to create a web application that
allows contractors to place orders without leaving their office. This is a first step toward
getting into the e-commerce space, and greatly expanding their potential client base.
Garden Center LLC has asked you to build out a REST API that can serve data to
customer facing mobile and web applications.

### Pre-requisites: 
none

### Usage:
This application will be used to manage the center's database

### Testing:
Please see the testing classes. Right-click the class test and choose "test with coverage"

### Linting:
various methods

#### Postman
https://api.postman.com/collections/24677641-f03146da-8f2e-4a91-95a2-faad1b9f1f2f?access_key=PMAT-01GSVWXGV2N35VGVH1KM5A6KAP
