package io.catalyte.dao;

import io.catalyte.entity.Office;

import java.util.List;

public interface IOfficeData {
    public List<Office> getOffices();
}
