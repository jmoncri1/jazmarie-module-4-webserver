package io.catalyte.dao;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter;
import io.catalyte.entity.Office;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OfficeData implements IOfficeData {
    public List<Office> getOffices() {
        String office = "{\"officeName\":\"Moncrieft Developments\",\"officeCity\":\"Charlotte\",\"officeState\":\"North Carolina\",\"officeDateEstablished\":\"2022-05-17T18:23:50.250Z\"}";

       Moshi moshi = new Moshi.Builder()
                .add(Date.class, new Rfc3339DateJsonAdapter().nullSafe())
                .build();
        JsonAdapter<Office> jsonAdapter = moshi.adapter(Office.class);

        List<Office> officeList = new ArrayList<>();
        try {
            officeList.add(jsonAdapter.fromJson(office));
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        officeList.stream().forEach(off -> System.out.println(off.toString()));

        return officeList;
    };
}
