package io.catalyte.controller;

import io.catalyte.entity.Employee;
import io.catalyte.service.EmployeeService;
import io.catalyte.service.EmployeeServiceImpl;

import java.util.List;

public class EmployeeController
{

    private EmployeeService employeeService;

    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }
    public void start(){
     employeeService.getEmployees();
    }
}
