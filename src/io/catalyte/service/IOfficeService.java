package io.catalyte.service;

import io.catalyte.dao.OfficeData;
import io.catalyte.entity.Office;

import java.util.List;

public interface IOfficeService {

    void getOffices();

}
