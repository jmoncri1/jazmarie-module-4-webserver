package io.catalyte.service;

import io.catalyte.dao.IOfficeData;
import io.catalyte.dao.OfficeData;
import io.catalyte.entity.Office;

import java.util.List;

public class OfficeService implements IOfficeService
{
    private IOfficeData iOfficeData;
    public void setIOfficeData(IOfficeData iOfficeData)
    {
        this.iOfficeData = iOfficeData;
    }

    public void getOffices()
    {
        iOfficeData.getOffices();
    }
}
