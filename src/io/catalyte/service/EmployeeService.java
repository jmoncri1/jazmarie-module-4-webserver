package io.catalyte.service;

import io.catalyte.dao.EmployeeCsvData;
import io.catalyte.entity.Employee;

import java.util.List;

public interface  EmployeeService
{
    void getEmployees();
}
