package io.catalyte.entity;

import java.util.Date;

public class Office {
    private String officeName;
    private String officeCity;
    private String officeState;
    private Date officeDateEstablished;
    public Office(String officeName, String officeCity, String officeState, Date officeDateEstablished) {
        this.officeName = officeName;
        this.officeCity = officeCity;
        this.officeState = officeState;
        this.officeDateEstablished = officeDateEstablished;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getOfficeCity() {
        return officeCity;
    }

    public void setOfficeCity(String officeCity) {
        this.officeCity = officeCity;
    }

    public String getOfficeState() {
        return officeState;
    }

    public void setOfficeState(String officeState) {
        this.officeState = officeState;
    }

    public Date getOfficeDateEstablished() {
        return officeDateEstablished;
    }

    public void setOfficeDateEstablished(Date officeDateEstablished) {
        this.officeDateEstablished = officeDateEstablished;
    }

    @Override
    public String toString() {
        String display = ("Office Name: " + officeName
                + "\nOffice City: " + officeCity
                + "\nOffice State: " + officeState
                + "\nEstablished on: " + officeDateEstablished);
        return display;
    }
}

