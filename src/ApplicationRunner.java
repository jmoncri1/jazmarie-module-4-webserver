import io.catalyte.controller.EmployeeController;
import io.catalyte.controller.OfficeController;
import io.catalyte.dao.*;
import io.catalyte.entity.Employee;
import io.catalyte.service.EmployeeServiceImpl;
import io.catalyte.service.OfficeService;

import java.util.List;

public class ApplicationRunner {

    public static void main (String[]args) {
        EmployeeController employeeController = new EmployeeController();
            EmployeeServiceImpl employeeServiceImpl = new EmployeeServiceImpl();
            EmployeeCsvData employeeCsvData = new EmployeeCsvData();
            EmployeeXmlData employeeXmlData = new EmployeeXmlData();

            employeeController.setEmployeeService(employeeServiceImpl);
            employeeServiceImpl.setEmployeeDao(employeeCsvData);

            employeeController.start();

            employeeServiceImpl.setEmployeeDao(employeeXmlData);

            employeeController.start();



            OfficeController officeController = new OfficeController();
            OfficeService officeService = new OfficeService();
            IOfficeData officeData = new OfficeData();

            officeController.setIOfficeService(officeService);
            officeService.setIOfficeData(officeData);

            officeController.start();

            }




    }
