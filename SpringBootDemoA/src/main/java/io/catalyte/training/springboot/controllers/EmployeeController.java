package io.catalyte.training.springboot.controllers;

import io.catalyte.training.springboot.entities.Employee;
import io.catalyte.training.springboot.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/employees")
public class EmployeeController {

    @Autowired
   public EmployeeService employeeService;

    @PostMapping
    public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee) {
        employeeService.addEmployee(employee);
        return new ResponseEntity<>(employee, HttpStatus.CREATED);
    }

    @GetMapping
    public List<Employee> getEmployees(@RequestParam Optional<Boolean> active) {
       if(active.isPresent()){
           return employeeService.getEmployees(active.get());
       }
        return employeeService.getEmployees();

    }

    @GetMapping(value = "/{employeeId}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable Long employeeId) {
        Employee employee = employeeService.getEmployeeById(employeeId);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @PutMapping(value = "/{employeeId}" )
    public Employee putEmployee(@RequestBody Employee employee, @PathVariable Long employeeId ) {
        employeeService.updateEmployee(employeeId, employee);
        return employee;
    }

    @DeleteMapping(value = "/{employeeId}" )
    public ResponseEntity<Employee> deleteEmployee(@PathVariable Long employeeId) {
        employeeService.deleteEmployee(employeeId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
