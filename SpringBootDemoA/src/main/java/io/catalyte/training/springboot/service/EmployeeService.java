package io.catalyte.training.springboot.service;

import io.catalyte.training.springboot.DAO.EmployeeDao;
import io.catalyte.training.springboot.entities.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeService {

    public EmployeeDao employeeDao = new EmployeeDao();

    // public Employee getEmployeeByFirstName(String firstName) {
    //      return employeeDao.findAll()
    //}

    public Employee getEmployeeByFirstName(String firstName) {
        for (Employee e : employeeDao.employees) {
            if (firstName.equals(e.getFirstName())) {
                return e;
            }
        }
        return null;
    }

    //public Employee getEmployeeById(Long employeeId) {
    //   return employeeDao.findById(employeeId).get();
    // }
    public Employee getEmployeeById(Long employeeId) {
        for (Employee e : employeeDao.employees) {
            if (employeeId.equals(e.getEmployeeId())) {
                return e;
            }
        }
        return null;
    }


    public void addEmployee(Employee employee) {
        employeeDao.employees.add(employee);
    }

    public List<Employee> getEmployees() {
        return employeeDao.employees;
    }

    public List<Employee> getEmployees(boolean isActive) {
        List<Employee> returnedEmployees = new ArrayList<>();
        for (Employee e : employeeDao.employees) {
            if (e.isActive() == isActive) {
                returnedEmployees.add(e);

            }
        }

        return returnedEmployees;
    }

    public Employee updateEmployee(Long employeeId, Employee employee) {
        Employee toBeUpdatedEmployee = getEmployeeById(employeeId);

        if (!employee.getFirstName().isEmpty()) {
            toBeUpdatedEmployee.setFirstName(employee.getFirstName());
        }
        if (!employee.getLastName().isEmpty()) {
            toBeUpdatedEmployee.setLastName(employee.getLastName());
        }
        if (!employee.getAge().isEmpty()) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        toBeUpdatedEmployee.setActive(employee.isActive());

        return toBeUpdatedEmployee;
    }

    public ResponseEntity<Employee> deleteEmployee(Long employeeId) {
        employeeDao.employees.remove(employeeId);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
