package io.catalyte.training.springboot.entities;

public class Employee {

    private Long employeeId;
    private String firstName;
    private String lastName;
    private String age;
    private String title;
    private boolean isActive;
    private Integer tenure;

    public Employee(Long employeeId, String firstName, String lastName, String age, String title, boolean isActive, Integer tenure) {
        this.employeeId = employeeId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.title = title;
        this.isActive = isActive;
        this.tenure = tenure;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Integer getTenure() {
        return tenure;
    }

    public void setTenure(Integer tenure) {
        this.tenure = tenure;
    }



    @Override
    public String toString(){
        return("Employee ID: " + employeeId
                + "\n" + "Employee First Name: " + firstName
                + "\n" + "Employee Last NAme: " + lastName
                + "\n" + "Employee Age: " + age
                + "\n" + "Employee Title: " + title
                + "\n" + "Active: " + isActive
                + "\n" + "Tenure: " + tenure
                );
    }
}

