package io.catalyte.training.springboot.DAO;

import io.catalyte.training.springboot.entities.Employee;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDao {


    public List<Employee> employees = new ArrayList<>();

    public EmployeeDao() {
        fillEmployeeList();
    }

    public void fillEmployeeList() {
        Employee employee1 = new Employee(1L, "Clark", "Kent", "30", "Superman", true, 50);
        Employee employee2 = new Employee(2L, "Bruce", "Wayne", "35", "Batman", false, 45);
        Employee employee3 = new Employee(3L, "Lex", "Luther", "40", "Villain", false, 30);
        Employee employee4 = new Employee(4L, "Loise", "Lane", "30", "News Anchor", true, 50);
        Employee employee5 = new Employee(5L, "Wells", "Kristin", "25", "Superwoman", true, 40);
        Employee employee6 = new Employee(6L, "Jazmarie", "Moncrieft", "22", "Software Developer", true
        , 1);

        employees.add(employee1);
        employees.add(employee2);
        employees.add(employee3);
        employees.add(employee4);
        employees.add(employee5);
        employees.add(employee6);
    }
}


