package io.catalyte.training.repositories;

import io.catalyte.training.entities.Pet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface PetRepository extends JpaRepository<Pet, Long> {
    
    List<Pet> findPetsByVaccination(List<Long> ids);


   boolean existByBreed(String breed);

   @Transactional
    void deleteByBreed(String breed);
}
