package io.catalyte.training.services;

import io.catalyte.training.entities.Pet;
import io.catalyte.training.entities.Vaccination;
import io.catalyte.training.repositories.PetRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Example;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


public class PetServiceImplTest {

    @Mock
    PetRepository petRepository;

    @Mock
    VaccinationService vaccinationService;

    @InjectMocks
    PetServiceImpl petServiceImpl;

    Vaccination testVaccine;

    Pet testPet;

    List<Pet> testList = new ArrayList<>();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        testVaccine = new Vaccination("Rabbies", Date.valueOf("2020-11-12"), testPet);
        testVaccine.setId(1L);

        testPet = new Pet("Spot", "Dalmatian", 2);
        testPet.setId(1L);
        testList.add(testPet);

        when(petRepository.findById(any(Long.class))).thenReturn(Optional.of(testList.get(0)));
        when(petRepository.saveAll(anyCollection())).thenReturn(testList);
        when(petRepository.save(any(Pet.class))).thenReturn(testList.get(0));
        when(petRepository.findAll()).thenReturn(testList);
        when(petRepository.findAll(any(Example.class))).thenReturn(testList);
    }

    @Test
    public void queryPets() {
        List<Pet> result = petServiceImpl.queryPets(new Pet());
        assertEquals(testList, result);
    }

   @Test
    public void getPet() {
        Pet result = petServiceImpl.getPet(1L);
        assertEquals(testPet, result);
    }

    @Test
    public void addPets() {
        List<Pet> result = petServiceImpl.addPets(testList);
        assertEquals(testList, result);
    }

    @Test
    public void addPet() {
        Pet result = petServiceImpl.addPet(testPet);
        assertEquals(testPet, result);
    }

    @Test
    public void updatePetById() {
        Pet result = petServiceImpl.updatePetById(1L, testPet);
        assertEquals(testPet, result);
    }

    @Test
    public void deletePet() {
        when(petRepository.existsById(anyLong())).thenReturn(true);
        petServiceImpl.deletePet(1L);
        verify(petRepository).deleteById(any());
    }





}

